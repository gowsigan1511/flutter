import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {
  String location;
  String time;
  String flag;
  String url;

  WorldTime({this.location, this.flag, this.url});
  // ignore: empty_constructor_bodies
  Future<void> getTime() async {
    Response response = await get('http://worldtimeapi.org/api/timezone/$url');
    Map data = jsonDecode(response.body);
    String dateTime = data['datetime'];
    DateTime now = DateTime.parse(dateTime);
    time = DateFormat.jm().format(now);
    //print(time);
  }
}
