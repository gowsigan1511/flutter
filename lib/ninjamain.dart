import 'package:flutter/material.dart';
import 'quote_card.dart';
import 'quote.dart';

void main() => runApp(MaterialApp(home: QuoteList()));

class QuoteList extends StatefulWidget {
  @override
  _QuoteListState createState() => _QuoteListState();
}

class _QuoteListState extends State<QuoteList> {
  List<Quote> quotes = [
    Quote(author: 'vindel', text: 'Be yourself dont change for others'),
    Quote(author: 'racko', text: 'Nerver change your attitude'),
    Quote(author: 'bravo', text: 'Be yourself dont change for others'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          title: Text('Title'),
          backgroundColor: Colors.redAccent,
        ),
        body: Column(
          children: quotes.map((quote) {
            return QuoteCard(
                quote: quote,
                delete: () {
                  setState(() => {quotes.remove(quote)});
                });
          }).toList(),
        ));
  }
}
