import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:myapp/pages/add_person.dart';
import 'package:myapp/pages/list.dart';

void main() async {
  await initHiveForFlutter();

  final HttpLink httpLink = HttpLink(
    'http://localhost:1337/graphql',
  );

  // final AuthLink authLink = AuthLink(
  //   //getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
  //   // OR
  //   getToken: () => 'Bearer b9915e5d91e061f8faf8cfcf6ddd77c3',
  // );

  final Link link = httpLink;

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      link: link,
      // The default store is the InMemoryStore, which does NOT persist to disk
      cache: GraphQLCache(store: HiveStore()),
    ),
  );

  runApp(GraphQLProvider(
    client: client,
    child: MaterialApp(initialRoute: '/', routes: {
      '/': (context) => ListWidget(),
      '/add_person': (context) => AddPerson(),
    }),
  ));
}
//void main() => runApp(DatePickerWidget());
