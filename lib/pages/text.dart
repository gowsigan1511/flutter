// import 'dart:ffi';

import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TextWidget extends StatelessWidget {
  bool isEmpty;
  String textLabel;
  Function getValue;

  TextWidget({this.textLabel, this.isEmpty, this.getValue});

  String buildValidator(String value) {
    if (value.isEmpty) {
      return '${this.textLabel} is required';
    }
    return null;
  }

  String emptyValidator(String value) {
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: Key(this.textLabel),
      decoration: InputDecoration(labelText: this.textLabel),
      // ignore: missing_return
      validator: (this.isEmpty) ? buildValidator : emptyValidator,
      onChanged: (String value) {
        this.getValue(value);
      },
    );
  }
}
