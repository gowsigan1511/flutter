import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:myapp/pages/core/popup_menu_button_wid.dart';

enum WhyFarther { view, edit, delete, id }

class ListWidget extends StatefulWidget {
  @override
  _ListWidgetState createState() => _ListWidgetState();
}

// ignore: must_be_immutable
class ListViewWidget extends StatelessWidget {
  List repositories;
  ListViewWidget(this.repositories);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: repositories.length,
        itemBuilder: (context, index) {
          final repository = repositories[index];
          return ListTile(
              leading: Image(image: AssetImage('assets/ninja.jpg')),
              title: Padding(
                padding: const EdgeInsets.fromLTRB(5, 5, 3, 3),
                child: Text(repository['person_name'],
                    style: TextStyle(fontSize: 22)),
              ),
              //subtitle: Text(repository['person_email']));
              subtitle: Align(
                alignment: Alignment.topLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 2, 2),
                          child: Icon(Icons.date_range_outlined,
                              color: Colors.blue.shade400, size: 16.0),
                        ),
                        Text(repository['date_of_birth'],
                            style: TextStyle(fontSize: 12)),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 2, 2),
                          child: Icon(Icons.mobile_friendly,
                              color: Colors.blue.shade400, size: 16.0),
                        ),
                        Text(repository['person_mobile'],
                            style: TextStyle(fontSize: 12)),
                      ],
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 2, 2),
                          child: Icon(Icons.mail,
                              color: Colors.blue.shade400, size: 16.0),
                        ),
                        Text(repository['person_email'],
                            style: TextStyle(fontSize: 15)),
                      ],
                    ),
                  ],
                ),
              ),
              trailing: PopupMenuButtonWidget(repository['id']));
        });
  }
}

class _ListWidgetState extends State<ListWidget> {
  String readRepositories = """
  query test() {
  people {
    id
    person_name
    person_email
    person_mobile
    date_of_birth
  } 
  }
""";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('List Page')),
        body: Column(
          children: [
            Flexible(
              child: Query(
                options: QueryOptions(
                  document: gql(readRepositories),
                ),
                builder: (QueryResult result,
                    {VoidCallback refetch, FetchMore fetchMore}) {
                  if (result.hasException) {
                    return Text(result.exception.toString());
                  }
                  //print(result);
                  if (result.isLoading) {
                    return Text('Loading');
                  }

                  List repositories = result.data['people'];
                  return ListViewWidget(repositories);
                },
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pushNamed(context, '/add_person');
            },
            child: Icon(Icons.add)),
        floatingActionButtonLocation: FloatingActionButtonLocation.startFloat);
  }
}
