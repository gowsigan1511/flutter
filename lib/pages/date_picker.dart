import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePickerWidget extends StatefulWidget {
  Function getValue;
  DatePickerWidget({this.getValue});
  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  final DateFormat formatter = DateFormat('dd/MM/yyyy');
  final DateFormat dbFormatter = DateFormat('yyyy-MM-dd');

  Future<void> buildDatePicker() async {
    print(selectedDate);
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        print(dbFormatter.format(picked));
        widget.getValue(dbFormatter.format(picked));
      });
  }

  DateTime selectedDate = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        key: Key(formatter.format(selectedDate).toString()),
        initialValue: formatter.format(selectedDate),
        decoration: InputDecoration(labelText: 'Date Of Birth'),
        onChanged: (String value) {
          widget.getValue(selectedDate);
        },
        onTap: () {
          buildDatePicker();
        });
  }
}
