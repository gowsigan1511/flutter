import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
//import 'package:intl/intl.dart';
import 'package:myapp/pages/date_picker.dart';
import 'package:myapp/pages/text.dart';
import 'package:myapp/pages/number.dart';

class AddPerson extends StatefulWidget {
  @override
  _AddPersonState createState() => _AddPersonState();
}

class _AddPersonState extends State<AddPerson> {
  final _formKey = GlobalKey<FormState>();
  String name;
  String phone;
  String date;
  String email;

  String addPeople = """
  mutation createPerson(\$person_name:String,\$person_mobile:String,\$person_email:String,\$date_of_birth:Date) {
    createPerson(input: {data:{person_name: \$person_name, person_mobile: \$person_mobile, person_email:\$person_email, date_of_birth:\$date_of_birth}}) {
      person {
        id
      }
    }
  }
""";

  @override
  initState() {
    super.initState();
  }

  getName(value) {
    this.name = value;
  }

  getPhone(value) {
    this.phone = value;
  }

  getEmail(value) {
    this.email = value;
  }

  getDate(value) {
    this.date = value;
    print(this.date);
  }

  defineMutationOptions() {
    return MutationOptions(
      document: gql(addPeople), // this is the mutation string you just created
      // you can update the cache based on results
      update: (GraphQLDataProxy cache, QueryResult result) {
        return cache;
      },
      // or do something with the result.data on completion
      onCompleted: (dynamic resultData) {
        Navigator.pushNamed(context, '/');
      },
    );
  }

  saveData() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
  }

  mutationButton(runMutation) {
    return FloatingActionButton.extended(
      onPressed: () {
        saveData();
        runMutation({
          'person_name': this.name,
          'person_mobile': this.phone,
          'person_email': this.email,
          'date_of_birth': this.date
        });
      },
      label: Text('create'),
      tooltip: 'Add',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Enter personel details')),
      body: Container(
        margin: EdgeInsets.all(21),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextWidget(
                textLabel: 'Name',
                isEmpty: true,
                getValue: getName,
              ),
              SizedBox(height: 20),
              NumberWidget(
                textLabel: 'Number',
                isEmpty: true,
                getValue: getPhone,
              ),
              SizedBox(height: 20),
              TextWidget(
                textLabel: 'Email',
                isEmpty: false,
                getValue: getEmail,
              ),
              SizedBox(height: 20),
              DatePickerWidget(
                getValue: getDate,
              ),
              SizedBox(height: 20),
              Mutation(
                options: defineMutationOptions(),
                builder: (
                  RunMutation runMutation,
                  QueryResult result,
                ) {
                  return mutationButton(runMutation);
                },
              ),
            ],
          ),
        ),
      ),
      // floatingActionButton: _buildButton(),
    );
  }
}
