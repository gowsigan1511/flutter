import 'package:flutter/material.dart';

// ignore: must_be_immutable
class NumberWidget extends StatelessWidget {
  String textLabel;
  bool isEmpty;
  Function getValue;
  NumberWidget({this.textLabel, this.isEmpty, this.getValue});

  String buildValidator(String value) {
    if (value.isEmpty) {
      return '${this.textLabel} is required';
    }
    return null;
  }

  String emptyValidator(String value) {
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: Key(this.textLabel),
      decoration: InputDecoration(labelText: this.textLabel),
      validator: (this.isEmpty) ? buildValidator : emptyValidator,
      onChanged: (String value) {
        getValue(value);
      },
    );
  }
}
