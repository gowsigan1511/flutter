import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data = {};

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context).settings.arguments;
    print(data);
    return Scaffold(
        body: Column(children: [
      FlatButton.icon(
          onPressed: () {
            Navigator.pushNamed(context, '/location');
          },
          label: Text('Edit Location'),
          icon: Icon(Icons.edit_location)),
      SizedBox(height: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(data['location'],
              style: TextStyle(fontSize: 20, letterSpacing: 2)),
        ],
      ),
      SizedBox(height: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(data['time'], style: TextStyle(fontSize: 50, letterSpacing: 2)),
        ],
      )
    ]));
  }
}
