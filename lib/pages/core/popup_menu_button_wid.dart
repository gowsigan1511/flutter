import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PopupMenuButtonWidget extends StatelessWidget {
  String id;
  PopupMenuButtonWidget(this.id);
  @override
  Widget build(BuildContext context) {
    id = this.id;
    return PopupMenuButton(
      onSelected: (result) {
        print(result);
      },
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          value: {'action': 'View', 'id': id},
          child: Text('View'),
        ),
        PopupMenuItem(
          value: {'action': 'Edit', 'id': id},
          child: Text('Edit'),
        ),
        PopupMenuItem(
          value: {'action': 'Delete', 'id': id},
          child: Text('Delete'),
        ),
      ],
    );
  }
}
